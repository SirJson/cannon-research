#ifndef LOGGER_H
#define LOGGER_H

void InitLogger(char* log = "engine.log");
void LogWriteSimple(const char* format, ...);
void LogWrite(const char* format, ...);
void LogWriteError(const char* format, ...);
void LogWriteWarning(const char* format, ...);

#endif // LOGGER_H
