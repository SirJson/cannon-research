/**
 * Client Data
 *
 * Ein Struct das die allgemeinen Daten eines Clienten speichert
 *
 * @author: Raffael
 * @package: NetShared
 * @since: 14.09.2011
 */

#include "DataTypes.h"
#include "Vector2.h"

struct ClientData
{
	ClientData()
	{
		ID = 0;
		Position = Vector2();
	}

	ClientData(Int32 id, Vector2 pos)
	{
		ID = id;
		Position = pos;
	}

	Int32 ID;
	Vector2 Position;
};