#ifndef PNGIMAGE_H
#define PNGIMAGE_H

#include "Image.h"

class PNGImage : public Image
{
    public:
        bool Load(const char* file);
    protected:
    private:
        unsigned int bytesPerPixel;
};

#endif // PNGIMAGE_H
