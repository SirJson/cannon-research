#ifndef TGAIMAGE_H
#define TGAIMAGE_H

#include "Image.h"

#ifdef WIN32
	#include <cstdio>
#else
	struct _IO_FILE;
	typedef struct _IO_FILE FILE;
#endif

class TGAImage : public Image
{
    public:
        bool Load(const char* file);
    protected:
    private:
        bool LoadUncompressed(FILE* file);
        bool LoadCompressed(FILE* file);
        unsigned char header[6];
        unsigned int bytesPerPixel;
        unsigned int imageSize;
};

#endif // TGAIMAGE_H
