#ifndef NETPROTOCOL_H
#define NETPROTOCOL_H

#include "DataTypes.h"

namespace ServerProtocol
{
	const Int32 Undefined = 0;
	const Int32 CMD_LandscapeTransmission = 1;
	const Int32 CMD_RemoveCircleFromTerrain = 2;
	const Int32 REQ_RemoveCircleFromTerrain  = 3;
	const Int32 REQ_MoveRight = 4;
	const Int32 REQ_MoveLeft = 5;
	const Int32 REQ_MoveUp = 6;
	const Int32 REQ_MoveDown = 7;
	const Int32 CMD_MoveRight = 8;
	const Int32 CMD_MoveLeft = 9;
	const Int32 CMD_MoveUp = 10;
	const Int32 CMD_MoveDown = 11;
	const Int32 REQ_MoveStop = 12;
	const Int32 CMD_MoveStop = 13;
	const Int32 INFO_ConnectionData = 14;
	const Int32 INFO_ClientConnected = 15;
	const Int32 INFO_ClientDisconnected = 16;
}

#endif // NETPROTOCOL_H