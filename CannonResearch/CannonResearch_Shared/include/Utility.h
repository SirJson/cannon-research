#ifndef UTILITY_H
#define UTILITY_H

struct Color;
class Color2DArray;

#include <string.h>
#include <string>
#include <sstream>
#include <cstdlib>

void InitRandom(unsigned int seed = 0);
inline float Clamp(float x, float a, float b);
template <typename T> T Random( const T& max )
{
	return T(rand()) * max / T(RAND_MAX + 1);
}

template <typename T> T RandomX( const T& min, const T& max)
{
	if(min < max)
		return T(rand()) * (max-min) / T(RAND_MAX) + min;
	else
		return T(rand()) * (min-max) / T(RAND_MAX) + max;
}

//! Constrains a value to a range
template <class T> T BoundBy(const T& Val, const T& Min, const T& Max)
{
	if(Val > Max)
		return Max;

	if(Val < Min)
		return Min;

	return Val;
}


//! Returns the smaller value
template <class T> T Min(const T& A, const T& B)
{
	return (A < B) ? A : B;
}


//! Returns the bigger value
template <class T> T Max(const T& A, const T& B)
{
	return (A > B) ? A : B;
}


//! Returns the absolute value
template <class T> T Abs(const T& Val)
{
	return (Val > 0) ? Val : -Val;
}


//! Tests if the value is inside a range
template <class T> bool Inside(const T& Val, const T& Min, const T& Max)
{
	return (Val >= Min) && (Val <= Max);
}


//! Constrains a value to a range by wrapping it
/*!
	Can be used for example to normalize Euler angles.
	Wrap(361, 0, 360) = 1
*/
template <class T> T Wrap(T Value, const T& Start, const T& End)
{
	if(!(End-Start)) return Start;

	while(Value < Start) Value += End - Start;
	while(Value >= End) Value -= End - Start;

	return Value;
}


//! Linear interpolation between two values
template <class T> T MixLinear(const T& A, const T& B, const float& F)
{
	return A + F * ( B - A );
}

template <typename T> T* SplitArray(T array[], int start, int end)
{
	T* arrayCpy = new T[end - start];
	memcpy(&arrayCpy[0],&array[start],(end - start) * sizeof(T));
	return arrayCpy;
}

template <typename T> T* SplitArraySequential(T array[], int start, int elementToCpyCount)
{
	T* arrayCpy = new T[elementToCpyCount];
	memcpy(&arrayCpy[0],&array[start],elementToCpyCount * sizeof(T));
	return arrayCpy;
}

template <typename T> std::string ValToString(T val)
{
	std::stringstream stream;
	stream << val;
	return stream.str();
}

#endif // UTILITY_H
