#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include "Color.h"
#include "Rectangle2D.h"

class Image
{
	public:
	Image();
	Image( const Image& source );
	Image(int bytesPerPixel, int width, int height);
	Image(Image& source, Rectangle2D area);

	Color* GetColorData() { return &colorData.front(); }
	int GetType() const;
	int GetInternalType() const;
	int GetWidth() const { return width; }
	int GetHeight() const { return height; }
	int GetBPP() const;
	int GetMipMapCount() const { return mipmapCount; }
	virtual bool Load(const char* file) { return false; }
	void Create(int bytesPerPixel, int width, int height);
	void SetImageData(unsigned char* data);
	void SetColorData(Color* data);
	unsigned char* GetPlainData();
	Color& At(int x, int y);

	protected:
		void BGRToRGB();
	
	std::vector<Color> colorData;
		int type, internalType;
		int width, height;
		int bitsPerPixel;
		int mipmapCount;
	private:
};

#endif // IMAGE_H
