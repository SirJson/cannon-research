#ifndef VECTOR2_H
#define VECTOR2_H

struct Vector2
{
	Vector2()
	{
		X = 0;
		Y = 0;
	}

    Vector2(float x, float y)
    {
        X = x;
        Y = y;
    }

    Vector2 operator+(const Vector2& lhs) const
    {
        Vector2 tmp(*this);
        tmp += lhs;
        return tmp;
    }

    Vector2& operator+=(const Vector2& rhs)
    {
        X += rhs.X;
        Y += rhs.Y;
        return *this;
    }

    Vector2 operator-(const Vector2& lhs) const
    {
        Vector2 tmp(*this);
        tmp -= lhs;
        return tmp;
    }

    Vector2& operator-=(const Vector2& rhs)
    {
        X -= rhs.X;
        Y -= rhs.Y;
        return *this;
    }

    float X;
    float Y;
};

#endif // VECTOR2_H
