#ifndef FOREACH_H
#define FOREACH_H

#include <boost/foreach.hpp>

#define foreach         BOOST_FOREACH
#define reverse_foreach BOOST_REVERSE_FOREACH

#endif // FOREACH_H

