#ifndef RECTANGLE_H
#define RECTANGLE_H

struct Vector2;

class Rectangle2D
{
public:
	Rectangle2D();
	Rectangle2D(int x, int y, int width, int height);
	int GetLeft() { return X; }
	int GetRight() { return X + Width; }
	int GetTop() { return Y; }
	int GetBottom() { return Y + Height; }
	void SetPosition(Vector2 pos);
	bool Contains(int x, int y);
	bool Contains(Vector2 value);
	bool Contains(Rectangle2D rectangle);
	bool Intersects(Rectangle2D r2);
	bool Intersects(int x, int y, int width, int height);
	Rectangle2D Fit( const Rectangle2D& area ) const;
	int X,Y;
	int Width,Height;
};

#endif // RECTANGLE_H
