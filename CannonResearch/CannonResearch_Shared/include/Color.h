#ifndef COLOR_H
#define COLOR_H

struct Color
{
    Color()
    {
        R = 0;
        G = 0;
        B = 0;
        A = 0;
    }

    Color(char r, char g, char b, char a)
    {
        R = r;
        G = g;
        B = b;
        A = a;
    }
    
    Color operator=(const Color& rhs)
    {
        R = rhs.R;
        G = rhs.G;
        B = rhs.B;
        A = rhs.A;
        return *this;
    }

    Color operator+(const Color& lhs) const
    {
        Color tmp(*this);
        tmp += lhs;
        return tmp;
    }

    Color& operator+=(const Color& rhs)
    {
        R += rhs.R;
        G += rhs.G;
        B += rhs.B;
        A += rhs.A;
        return *this;
    }

    Color operator-(const Color& lhs) const
    {
        Color tmp(*this);
        tmp -= lhs;
        return tmp;
    }

    Color& operator-=(const Color& rhs)
    {
        R -= rhs.R;
        G -= rhs.G;
        B -= rhs.B;
        A -= rhs.A;
        return *this;
    }

    char R,G,B,A;
};

#endif // COLOR_H
