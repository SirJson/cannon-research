#ifndef VERIFY_H
#define VERIFY_H

#include <cstdio>
#ifdef WIN32
#include <Windows.h>
#else
#include <signal.h>
#endif

#if defined(WIN32) && defined(_DEBUG)
#define verify(f,msg,...)((void) ((f) || (ShowVerifyError(msg,__FILE__,__LINE__,__VA_ARGS__),DebugBreak(), 0)))
#else
#define verify(f,msg,...)((void) ((f) || (ShowVerifyError(msg,__FILE__,__LINE__,__VA_ARGS__),raise(SIGTRAP), 0)))
#endif

#ifdef WIN32
void ShowVerifyError(const char* message, const char* file, int line, ...);
#else
void ShowVerifyError(char const* message, char const* file, int line, ...);
#endif

#endif //VERIFY_H