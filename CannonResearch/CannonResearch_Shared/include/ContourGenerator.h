#ifndef CONTOURGENERATOR_H
#define CONTOURGENERATOR_H
#include <vector>
#include "DataTypes.h"

using namespace std;

class ContourGenerator
{
    public:
	ContourGenerator();
	ContourGenerator(int width, int height,float peakheight, float flatness);
	virtual ~ContourGenerator();

	int GetWidth() const { return width; }
	int GetHeight() const { return height; }
	std::vector<Int32> GetTerrainContour() { return terrainContour; }

    private:
	void GenerateTerrainContour(float peakheight, float flatness);

	std::vector<Int32> terrainContour;
	int width, height;
};

#endif // CONTOURGENERATOR_H
