#include "Rectangle2D.h"
#include "Vector2.h"

Rectangle2D::Rectangle2D()
{
	X = 0;
	Y = 0;
	Width = 0;
	Height = 0;
}

Rectangle2D::Rectangle2D(int x, int y, int width, int height)
{
	X = x;
	Y = y;
    Width = width;
    Height = height;
}

void Rectangle2D::SetPosition(Vector2 pos)
{
	X = pos.X; 
	Y = pos.Y; 
}

bool Rectangle2D::Contains(int x, int y)
{
	return ((((X <= x) && (x < (X + Width))) && (Y <= y)) && (y < (Y + Height)));
}

bool Rectangle2D::Contains(Vector2 value)
{
	return ((((X <= value.X) && (value.X < (X + Width))) && (Y <= value.Y)) && (value.Y < (Y + Height)));
}

bool Rectangle2D::Contains(Rectangle2D value)
{
	return ((((X <= value.X) && ((value.X + value.Width) <= (X + Width))) && (Y <= value.Y)) && ((value.Y + value.Height) <= (Y + Height)));	
}

bool Rectangle2D::Intersects(Rectangle2D r2)
{
	return !(r2.GetLeft() > GetRight()
			|| r2.GetRight() < GetLeft()
            || r2.GetTop() > GetBottom()
            || r2.GetBottom() < GetTop()
            );
}

bool Rectangle2D::Intersects(int x, int y, int width, int height)
{
	return Intersects(Rectangle2D(x,y,width,height));
}

Rectangle2D Rectangle2D::Fit( const Rectangle2D& area ) const
{
	Rectangle2D r = area;
	
	if(area.X < X)
		r.X = X;

	if(area.Y < Y)
		r.Y = Y;
	
	if(area.X+area.Width > X+Width)
		r.Width = (X+Width)-area.X;

	if(area.Y+area.Height > Y+Height)
		r.Height = (Y+Height)-area.Y;
	
	return r;
}

