#include "Logger.h"
#ifdef WIN32
	#include "Windows.h"
	#include <string>
	#include "Utility.h"
#else
	#include <cstdio>
#endif

#ifdef WIN32
void ShowVerifyError(const char* message, const char* file, int line, ...)
#else
void ShowVerifyError(char const* message, char const* file, int line, ...)
#endif
{
	LogWriteSimple("ERROR: Verify failed(File: %s Line: %i) -> ", file, line);
	LogWriteSimple("%s",message);
	LogWriteSimple("\n");
#ifdef WIN32
	MessageBoxA(NULL,(std::string(message) + "\n\nFile: " + std::string(file) + "\nLine: " + ValToString<int>(line)).c_str(),"Verify failed!",NULL);
#else
	printf("\a");
	printf("Press 'Enter' to continue ...\n");
    while (getchar() != '\n')
        ;
#endif
}