#include <cstdio>
#include <cstdarg>
#include <string>

char* logName = "engine.log";

void InitLogger(char* log)
{
	logName = log;
    remove(logName);
}

void LogWriteSimple(const char* format, ...)
{
	FILE* file = fopen(logName,"a+");
	va_list vl;
	va_start(vl, format);
	vprintf(format,vl);
    vfprintf(file,format,vl);
	va_end(vl);
    fclose(file);
}

void LogWrite(const char* format, ...)
{
	FILE* file = fopen(logName,"a+");
	va_list vl;
	va_start(vl, format);
	vprintf(format,vl);
	printf("\n");
	if(file != NULL) // TODO: Das Problem mal richtig l�sen >:O u nub!
	{
		vfprintf(file,format,vl);
		fprintf(file,"\n");
	}
	va_end(vl);
    fclose(file);
}

void LogWriteError(const char* format, ...)
{
	FILE* file = fopen(logName,"a+");
	va_list vl;
	va_start(vl, format);
	printf("ERROR: ");
	vprintf(format,vl);
	printf("\n");
    vfprintf(file,format,vl);
	fprintf(file,"\n");
	va_end(vl);
    fclose(file);
}

void LogWriteWarning(const char* format, ...)
{
	FILE* file = fopen(logName,"a+");
	va_list vl;
	va_start(vl, format);
	printf("WARNING: ");
	vprintf(format,vl);
	printf("\n");
    vfprintf(file,format,vl);
	fprintf(file,"\n");
	va_end(vl);
    fclose(file);
}
