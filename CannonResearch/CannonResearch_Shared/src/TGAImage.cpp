#include "TGAImage.h"
#include <stdlib.h>
#include <cstring>
#include <cstdio>
#ifdef WIN32
	#include <Windows.h>
#endif
#include <GL/gl.h>
#include "Logger.h"

typedef struct
{
    GLubyte Header[12];	 // File Header To Determine File Type
} TGAHeader;

bool TGAImage::Load(const char* file)
{
    TGAHeader tgaHeader;
	// Uncompressed TGA Header
	GLubyte uTGAHeader[12] = {0,0,2,0,0,0,0,0,0,0,0,0};
	// Compressed TGA Header
	GLubyte cTGAHeader[12] = {0,0,10,0,0,0,0,0,0,0,0,0};
    FILE* texFile = fopen(file,"rb");
    if(texFile == NULL)
        return false; // Epic fail X_x
    if(fread(&tgaHeader, sizeof(TGAHeader), 1, texFile) == 0)
        return false;
	// memcmp is irgendwie Praktisch wenn man C# kennt :D (Da sollte man die Inhalte von 2 Arrays am besten über LINQ Vergleichen :O Overhead pur)
	if(memcmp(uTGAHeader, &tgaHeader, sizeof(tgaHeader)) == 0)
        return LoadUncompressed(texFile);
	else if(memcmp(cTGAHeader, &tgaHeader, sizeof(tgaHeader)) == 0)
		return LoadCompressed(texFile);
    return false;
}

bool TGAImage::LoadCompressed(FILE* file)
{
	if(fread(header, sizeof(header), 1, file) == 0)
		return false;
	width = header[1] * 256 + header[0];
	height = header[3] * 256 + header[2];
	bitsPerPixel = header[4];
	if(width <= 0 || height <= 0)
		return false;
	if(bitsPerPixel == 24)
		type = GL_RGB;
	else
		type = GL_RGBA;
	bytesPerPixel = bitsPerPixel / 8;
	colorData.resize(width*height);

	GLuint pixelcount = height * width;	// Number Of Pixels In The Image
	GLuint currentpixel	= 0;			// Current Pixel We Are Reading From Data
	GLuint currentbyte	= 0;			// Current Byte We Are Writing Into Imagedata
	unsigned char* colorBytes = (unsigned char*)&colorData.front();
	
	// Storage For 1 Pixel
	GLubyte * colorbuffer = (GLubyte *)malloc(bytesPerPixel);
	do
	{
		GLubyte chunkheader = 0;			// Variable To Store The Value Of The Id Chunk
		if(fread(&chunkheader, sizeof(GLubyte), 1, file) == 0)	// Attempt To Read The Chunk's Header.
			return false;				// If It Fails, Return False
	
		if(chunkheader < 128)				// If The Chunk Is A 'RAW' Chunk
		{
			chunkheader++;				// Add 1 To The Value To Get Total Number Of Raw Pixels
			// Start Pixel Reading Loop
			for(short counter = 0; counter < chunkheader; counter++)
			{
				// Try To Read 1 Pixel
				if(fread(colorbuffer, 1, bytesPerPixel, file) != bytesPerPixel)
					return false;			// If It Fails, Return False
				colorBytes[currentbyte] = colorbuffer[2];		// Write The 'R' Byte
				colorBytes[currentbyte + 1	] = colorbuffer[1];	// Write The 'G' Byte
				colorBytes[currentbyte + 2	] = colorbuffer[0];	// Write The 'B' Byte
				if(bytesPerPixel == 4)					// If It's A 32bpp Image...
					colorBytes[currentbyte + 3] = colorbuffer[3];	// Write The 'A' Byte
				else
					colorBytes[currentbyte + 3] = 255;
				// Increment The Byte Counter By The Number Of Bytes In A Pixel
				currentbyte += bytesPerPixel;
				currentpixel++;					// Increment The Number Of Pixels By 1
			}
		}
		else // If It's An RLE Header
		{
			chunkheader -= 127;			// Subtract 127 To Get Rid Of The ID Bit
			// Read The Next Pixel
			if(fread(colorbuffer, 1, bytesPerPixel, file) != bytesPerPixel)
				return false;				// If It Fails, Return False
			// Start The Loop
			for(short counter = 0; counter < chunkheader; counter++)
			{
				// Copy The 'R' Byte
				colorBytes[currentbyte] = colorbuffer[2];
				// Copy The 'G' Byte
				colorBytes[currentbyte + 1	] = colorbuffer[1];
				// Copy The 'B' Byte
				colorBytes[currentbyte + 2	] = colorbuffer[0];
				if(bytesPerPixel == 4)		// If It's A 32bpp Image
					colorBytes[currentbyte + 3] = colorbuffer[3];
				else
					colorBytes[currentbyte + 3] = 255;
				currentbyte += bytesPerPixel;	// Increment The Byte Counter
				currentpixel++;				// Increment The Pixel Counter
			}
		}
	}
	while(currentpixel < pixelcount);

	internalType = bytesPerPixel;
	fclose(file);
	LogWriteSimple("Sucessful!\n");
	return true;
}

bool TGAImage::LoadUncompressed(FILE* file)
{
// Fuck NO! Henry is too dumm for this.
// He gives a shit and comments ALL the things out!
/*
    if(fread(header, sizeof(header), 1, file) == 0)
        return false;
    width = header[1] * 256 + header[0];
    height = header[3] * 256 + header[2];
    bitsPerPixel = header[4];
    if(width <= 0 || height <= 0)
		return false;
    if(bitsPerPixel == 24)
		type = GL_RGB;
	else
		type = GL_RGBA;
    bytesPerPixel = bitsPerPixel / 8;
    imageSize = bytesPerPixel * width * height;
    imageData = (GLubyte*)malloc(imageSize); // Allocate Memory
    // unsigned char* colorBytes = (unsigned char*)&colorData.first();

    if(fread(imageData, 1, imageSize, file) != imageSize)
        return false;
    // Sweap BGR to RGB
	BGRToRGB();
	internalType = bytesPerPixel;
	fclose(file);
	SyncColorData();
	LogWriteSimple("Sucessful!\n");
*/
    return true;
}
