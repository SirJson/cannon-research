#include "ContourGenerator.h"
#include <cmath>
#include "Utility.h"
#include <stdlib.h>

ContourGenerator::ContourGenerator()
{
	this->width = 0;
	this->height = 0;
}

ContourGenerator::ContourGenerator(int width, int height,float peakheight, float flatness)
{
    this->width = width;
    this->height = height;
	GenerateTerrainContour(peakheight,flatness);
}

ContourGenerator::~ContourGenerator()
{
}

void ContourGenerator::GenerateTerrainContour(float peakheight, float flatness)
{
	terrainContour.resize(width);

	double rand1 = RandomX(0.0,1.0) + 1;
	double rand2 = RandomX(0.0,1.0) + 2;
	double rand3 = RandomX(0.0,1.0) + 3;

	float offset = (float)height / 2;

	for (int x = 0; x < width; x++)
	{
		double height = peakheight / rand1 * sin((float)x / flatness * rand1 + rand1);
		height += peakheight / rand2 * sin((float)x / flatness * rand2 + rand2);
		height += peakheight / rand3 * sin((float)x / flatness * rand3 + rand3);
		height += offset;
		terrainContour[x] = (int)height;
	}
}
