#include "Foreach.h"
#include "Image.h"
#include <assert.h>
#ifdef WIN32
	#include <Windows.h>
#endif
#include <GL/gl.h>
#include "Logger.h"
#include <string.h>

using namespace std;


Image::Image()
{
	width = 0;
	height = 0;
	internalType = 0;
	bitsPerPixel = 0; // nur wichtig bei Loadern
	type = 0;
	mipmapCount = 0;
}

// TODO: Copy Constructor wird eigentlich nicht mehr gebraucht, da die Klasse keine Pointer mehr nutzt.
Image::Image( const Image& source ) // Copy Constructor
{
	bitsPerPixel = source.GetBPP();
	height = source.GetHeight();
	width = source.GetWidth();
	internalType = source.GetInternalType();
	mipmapCount = source.GetMipMapCount();
	type = source.GetType();
	colorData = source.colorData;
}

Image::Image(int bytesPerPixel, int width, int height)
{
	Create(bytesPerPixel,width,height);
}

Image::Image(Image& source, Rectangle2D area) // Henry's Patentierter Image zerschnippel Constructor
{ 
	LogWriteSimple("Create new Image from Source...");
	Rectangle2D sourceArea(0,0,source.GetWidth(),source.GetHeight());
	area = sourceArea.Fit(area);
	assert(sourceArea.Contains(area));
	Create(source.GetInternalType(), area.Width, area.Height);
	for( int x = 0; x < area.Width; x++ )
		for( int y = 0; y < area.Height; y++ )
		{
			At(x,y) = source.At(area.X + x, area.Y + y);
			//LogWriteSimple(".");
		}
	LogWriteSimple("Finished!\n");
}

void Image::BGRToRGB()
{
	unsigned char swap;
	foreach(vector<Color>::value_type& i, colorData)
	{
		swap = i.R;
		i.R = i.B;
		i.B = swap;
	}
}

void Image::Create(int bytesPerPixel, int width, int height)
{
	colorData.resize(width*height);
	this->width = width;
	this->height = height;
	internalType = bytesPerPixel;
	if(bytesPerPixel == 3)
		type = GL_RGB;
	else if(bytesPerPixel == 4)
		type = GL_RGBA;
	mipmapCount = 0;
	bitsPerPixel = 0;
}

Color& Image::At(int x, int y)
{
	assert(x <= width);
	assert(y <= height);
	return colorData[x + y*width];
}

int Image::GetType() const
{
	return GL_RGBA;
// 	return type;
}

int Image::GetInternalType() const
{
	return internalType;
}

int Image::GetBPP() const
{
	return 32; // TODO: Texture2D muss Bilder mit *Stride* laden können.
// 	return bitsPerPixel;
}

unsigned char* Image::GetPlainData()
{
	unsigned char* data = new unsigned char[width*height*internalType];
	int colorsCount = width*height;
	int num = 0;
	for(int i = 0; i != width*height*internalType; i+=internalType)
	{
		Color col = colorData[num];
		if(internalType == 4)
		{
			data[i] = col.R;
			data[i+1] = col.G;
			data[i+2] = col.B;
			data[i+3] = col.A;
		}
		else
		{
			data[i] = col.R;
			data[i+1] = col.G;
			data[i+2] = col.B;
		}
		num++;
	}
	return data;
}

void Image::SetImageData(unsigned char* data)
{
	int num = 0;
	for(int i = 0; i != width*height*internalType; i+=internalType)
	{
		if(internalType == 4)
		{
			colorData[num].R = data[i];
			colorData[num].G = data[i+1];
			colorData[num].B = data[i+2];
			colorData[num].A = data[i+3];
		}
		else
		{
			colorData[num].R = data[i];
			colorData[num].G = data[i+1];
			colorData[num].B = data[i+2];
		}
		num++;
	}
}