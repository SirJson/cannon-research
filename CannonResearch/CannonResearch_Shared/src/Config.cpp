#include "Config.h"
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <vector>

Config::Config()
{

}

void Config::Load(std::string file)
{
	configValue.clear();
	std::ifstream fileHandle;
	fileHandle.open(file.c_str());
	std::string line;
	while(std::getline(fileHandle, line))
	{
		std::vector<std::string> strs;
		boost::split(strs, line, boost::is_any_of("="));
		configValue[strs[0]] = strs[1];
	}
}

bool Config::GetBoolValue(std::string key)
{
	if(configValue[key] == "true")
		return true;
	else
		return false;
}