#include <cstdlib>
#include <ctime>
#include "Color.h"

void InitRandom(unsigned int seed)
{
	if(!seed)
		seed = (unsigned int)time(0);
	srand(seed);
}

inline float Clamp(float x, float a, float b)
{   
	return x < a ? a : (x > b ? b : x);
}