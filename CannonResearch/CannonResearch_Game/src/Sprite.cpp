#include "Sprite.h"
#ifdef WIN32
	#include <Windows.h>
#endif
#include <GL/gl.h>
#include "Vector2.h"
#include "Texture2D.h"
#include "Size2D.h"
#include "Logger.h"

Sprite::Sprite()
{
	_tex = NULL;
	_size = Size2D(0,0);
	_pos = Vector2(0,0);
	_bounds = Rectangle2D(0,0,0,0);
	enabled = false;
}

Sprite::Sprite(Texture2D* texture)
{
	_tex = texture;
    _pos = Vector2(0,0);
    _size = Size2D(_tex->GetWidth(),_tex->GetHeight());
	_bounds = Rectangle2D(0,0,_tex->GetWidth(),_tex->GetHeight());
	enabled = true;
}

Sprite::Sprite(Texture2D* texture,Vector2 pos)
{
    _tex = texture;
    _pos = pos;
    _size = Size2D(_tex->GetWidth(),_tex->GetHeight());
	_bounds = Rectangle2D(pos.X,pos.Y,_tex->GetWidth(),_tex->GetHeight());
	enabled = true;
}

Sprite::~Sprite()
{
    delete _tex;
}

void Sprite::SetActive(bool mode)
{
	enabled = mode;
}

void Sprite::SetSize(Size2D size)
{
	_size = size;
}

void Sprite::Draw()
{
	if(enabled)
	{
		glBindTexture(GL_TEXTURE_2D,_tex->GetHandle());
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(0 + _pos.X, _size.Height + _pos.Y, 0);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(_size.Width + _pos.X, _size.Height + _pos.Y, 0);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(_size.Width + _pos.X, 0 + _pos.Y, 0);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0 + _pos.X, 0 + _pos.Y, 0);
		glEnd();
	}

}

void Sprite::SetPosition(Vector2 pos)
{
    _pos = pos;
	_bounds.SetPosition(pos);
}

Size2D Sprite::GetSize()
{
    return _size;
}

Vector2 Sprite::GetPosition()
{
    return _pos;
}
