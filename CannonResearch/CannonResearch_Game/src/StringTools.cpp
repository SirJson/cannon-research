#include "StringTools.h"

#include <cstdarg>
#include <sstream>
#include <cstring>

using namespace std;

namespace tool
{
namespace sz
{
	/// C-Strings

bool SEqual(const char* A, const char* B, int iMaxLength, bool bIgnoreCase)
{
	for(int i = 0; i <= iMaxLength; i++)
	{
		char a;
		char b;

		if(!bIgnoreCase)
		{
			a = *(A+i);
			b = *(B+i);
		}
		else
		{
			a = toupper(*(A+i));
			b = toupper(*(B+i));
		}

		if(a == b)
		{
			if(a != '\0')
				continue;
			else
				break;
		}
		return false;
	}
	return true;
}

char* StringToChar(std::string string)
{
    char* sz;
    sz = new char[string.length() + 1];
    strcpy(sz, string.c_str());
    return sz;
}

bool WildCardMatch(const char* pattern, const char* compare)//by Alessandro Cantatore
{
	char* Pattern = (char*)pattern;
	char* Compare = (char*)compare;
	char* s;
	char* p;
	bool star = false;

loopStart:
	 for(s = Compare, p = Pattern; *s; ++s, ++p)
	 {
			switch(*p)
			{
				 case '?':
						if (*s == '.') goto starCheck;
						break;

				 case '*':
						star = true;
						Compare = s;
						Pattern = p;
						if(!*++Pattern) return true;
						goto loopStart;

				 default:
						if(toupper(*s) != toupper(*p))
							 goto starCheck;
						break;

			}
	 }

	 if(*p == '*') ++p;
	 return !*p;

starCheck:
	 if(!star) return false;
	 Compare++;
	 goto loopStart;
}

bool WildCardMatch(char* Pattern, char* Compare)//by Alessandro Cantatore
{
	char* s;
	char* p;
	bool star = false;

loopStart:
	 for(s = Compare, p = Pattern; *s; ++s, ++p)
	 {
			switch(*p)
			{
				 case '?':
						if (*s == '.') goto starCheck;
						break;

				 case '*':
						star = true;
						Compare = s;
						Pattern = p;
						if(!*++Pattern) return true;
						goto loopStart;

				 default:
						if(toupper(*s) != toupper(*p))
							 goto starCheck;
						break;

			}
	 }

	 if(*p == '*') ++p;
	 return !*p;

starCheck:
	 if(!star) return false;
	 Compare++;
	 goto loopStart;
}


	/// C++-Strings

typeFn* isalnum = std::isalnum;
typeFn* isalpha = std::isalpha;
typeFn* iscntrl = std::iscntrl;
typeFn* isdigit = std::isdigit;
typeFn* isgraph = std::isgraph;
typeFn* islower = std::islower;
typeFn* isprint = std::isprint;
typeFn* ispunct = std::ispunct;
typeFn* isspace = std::isspace;
typeFn* isupper = std::isupper;
typeFn* isxdigit = std::isxdigit;


bool contains(string& String, string& Chars)
{
	return String.find_first_of(Chars, 0) != string::npos;
}

bool containsFn(string& String, const size_t Count, ...)
{
	typeFn** fnv = new typeFn*[Count];

	va_list vl;
	va_start(vl, Count);

		for(size_t i = 0; i < Count; i++)
			fnv[i] = (typeFn*)(va_arg(vl, void*));

	va_end(vl);

	bool ret = containsFnV(String, Count, fnv);
	delete fnv;
	return ret;
}

bool containsFnV(string& String, size_t Count, typeFn** FnV)
{
	string::const_iterator i = String.begin();
	size_t j;
	for(; i != String.end(); i++)
	{
		for(j = 0; j < Count; j++)
			if( FnV[j](*i) )
				return true;
	}
	return false;
}


size_t findFirstOfFn(string String, size_t Pos, size_t Count, ...)
{
	typeFn** fnv = new typeFn*[Count];

	va_list vl;
	va_start(vl, Count);

		for(size_t i = 0; i < Count; i++)
			fnv[i] = (typeFn*)(va_arg(vl, void*));

	va_end(vl);

	size_t ret = findFirstOfFnV(String, Pos, Count, fnv);
	delete fnv;
	return ret;
}

size_t findFirstOfFnV(string String, size_t Pos, size_t Count, typeFn** FnV)
{
	string::const_iterator i = String.begin()+Pos;
	size_t j;
	for(; i < String.end(); i++)
	{
		for(j = 0; j < Count; j++)
			if( FnV[j](*i) )
				return j;
	}
	return string::npos;
}

size_t findFirstNotOfFn(string String, size_t Pos, size_t Count, ...)
{
	typeFn** fnv = new typeFn*[Count];

	va_list vl;
	va_start(vl, Count);

		for(size_t i = 0; i < Count; i++)
			fnv[i] = (typeFn*)(va_arg(vl, void*));

	va_end(vl);

	size_t ret = findFirstOfFnV(String, Pos, Count, fnv);
	delete fnv;
	return ret;
}

size_t findFirstNotOfFnV(string String, size_t Pos, size_t Count, typeFn** FnV)
{
	string::const_iterator i = String.begin()+Pos;
	size_t j;
	for(; i < String.end(); i++)
	{
		for(j = 0; j < Count; j++)
			if( !FnV[j](*i) )
				return j;
	}
	return string::npos;
}


void replaceAll(string& String, string& Find, string& Replace)
{
	size_t pos = size_t(-1);
	while((pos = String.find(Find,pos+1)) != string::npos)
		String.replace(pos,Find.length(),Replace);
}

void replaceAllChars(string& String, string& Find, string& Replace)
{
	string::iterator it = String.begin();
	size_t pos;

	for(; it < String.end(); it++)
	{
		for(pos = 0; pos <= Find.length(); pos++)
		{
			if((*it) == Find[pos])
			{
				(*it) = Replace[pos];
				break;
			}
		}
	}
}

void replaceAllChars(string& String, string& Find, const char& Replace)
{
	string::iterator it = String.begin();
	size_t pos;

	for(; it < String.end(); it++)
	{
		for(pos = 0; pos <= Find.length(); pos++)
		{
			if((*it) == Find[pos])
			{
				(*it) = Replace;
				break;
			}
		}
	}
}

void replaceAllChars(string& String, const char& Find, const char& Replace)
{
	string::iterator it = String.begin();
	for(; it < String.end(); it++)
	{
		if((*it) == Find)
			*it = Replace;
	}
}

void removeAll(string& String, string& Find)
{
	size_t pos = size_t(-1);
	while((pos = String.find(Find,pos+1)) != string::npos)
		String.erase(pos,Find.length());
}

void removeAllChars(string& String, string& Find)
{
	string::iterator it = String.begin();
	size_t pos;

	for(; it < String.end(); it++)
	{
		for(pos = 0; pos <= Find.length(); pos++)
		{
			if((*it) == Find[pos])
			{
				it = String.erase(it);
				break;
			}
		}
	}
}

void trim(string& String, string& Chars)
{
	size_t pos;

	// Vorne
	pos = String.find_first_not_of(Chars,0);
	if(pos != string::npos) String.erase(0,pos);

	// Hinten
	pos = String.find_last_not_of(Chars,string::npos);
	if(pos != string::npos) String.erase(pos+1,string::npos);
}

size_t split(const string& In, string& Out, const size_t& iPos, const char& iDelim)
{
	size_t np = In.find_first_of(iDelim, iPos);
	Out = In.substr(iPos, np-iPos);
	return (np >= string::npos) ? np : np+1;
}

bool toBool(const string& String)
{
	return (String != "0") && (String != "false");
}

int toInt(const string& String)
{
	istringstream iStream(String);
	int i;
	iStream >> i;
	return i;
}

float toFloat(const string& String)
{
	istringstream iStream(String);
	float f;
	iStream >> f;
	return f;
}

void toUpper(string& String)
{
	string::iterator it;
	for(it = String.begin(); it < String.end(); it++)
		(*it) = toupper(*it);
}

void toLower(string& String)
{
	string::iterator it;
	for(it = String.begin(); it < String.end(); it++)
		(*it) = tolower(*it);
}

}
}
