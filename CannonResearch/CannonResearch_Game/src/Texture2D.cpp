#include "Texture2D.h"
#ifdef WIN32
	#include <Windows.h>
#endif
#include <GL/gl.h>
#include "StringTools.h"
#include "TGAImage.h"
#include "PNGImage.h"
#include <cstdio>
#include "Logger.h"

Texture2D::Texture2D(Image* image, bool useMipMaps, bool niceFiltering)
{
	this->image = image;
	LogWrite("Pointer Value: %p",this->image);
	glGenTextures(1,&handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	if(useMipMaps == true)
	{
#ifndef WIN32
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, true);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
#endif
	}
	else
	{
		if(niceFiltering)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}
	}
#ifndef WIN32
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
#endif
	
	
	glTexImage2D(GL_TEXTURE_2D, 0, image->GetInternalType(), image->GetWidth(),image->GetHeight(), 0, image->GetType(), GL_UNSIGNED_BYTE, image->GetColorData());
	width = image->GetWidth();
	height = image->GetHeight();
}

Texture2D::Texture2D(unsigned char* data, int width, int height, int channels, bool linearFilter)
{
	LogWriteSimple("Loading Data from Memory... ");
	glGenTextures(1,&handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	if(linearFilter)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
#ifndef WIN32
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
#endif
	if(channels == 3)
		glTexImage2D(GL_TEXTURE_2D, 0, channels, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	if(channels == 4)
		glTexImage2D(GL_TEXTURE_2D, 0, channels, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	this->width = width;
	this->height = height;
	LogWriteSimple("Sucessfull!\n");
}

Texture2D::Texture2D(std::string file, bool useMipMaps)
{
	LogWrite("Loading %s ...", file.c_str());
	if(tool::sz::WildCardMatch("*.tga",file.c_str()))
		image = new TGAImage();
	if(tool::sz::WildCardMatch("*.png",file.c_str()))
		image = new PNGImage();
	if(image == NULL)
	{
		LogWriteSimple("Failed! (Unknown File Type)\n");
		return;
	}
	if(!image->Load(file.c_str()))
	{
		LogWriteSimple("Failed!");
		return;
	}
	glGenTextures(1,&handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	if(useMipMaps == true)
	{
#ifndef WIN32
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, true);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
#endif
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
#ifndef WIN32
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
#endif
	glTexImage2D(GL_TEXTURE_2D, 0, image->GetInternalType(), image->GetWidth(),image->GetHeight(), 0, image->GetType(), GL_UNSIGNED_BYTE, image->GetColorData());
	width = image->GetWidth();
	height = image->GetHeight();
}

Texture2D::~Texture2D()
{
    glDeleteTextures(1, &handle);
}

void Texture2D::UpdateInfo()
{
	glBindTexture(GL_TEXTURE_2D, handle);
	//glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, image->GetWidth(),image->GetHeight(), image->GetType(), GL_UNSIGNED_BYTE, image->GetColorData());
	glTexImage2D(GL_TEXTURE_2D, 0, image->GetInternalType(), image->GetWidth(),image->GetHeight(), 0, image->GetType(), GL_UNSIGNED_BYTE, image->GetColorData());
}
