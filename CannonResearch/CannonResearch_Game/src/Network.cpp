#include "Network.h"
#include "EngineConfig.h"
#include "Logger.h"
#include "Verify.h"
#include "NetProtocol.h"
#include "Image.h"
#include "Packet.h"
#include "GameEngine.h"

Network::Network()
{
	client = NULL;
	server = NULL;
	connected = false;
	updateLoopRunning = false;
	localPlrID = -1;
}

Network::~Network()
{
	updateLoopRunning = false;
	if(client != NULL)
		enet_host_destroy(client);
}

void Network::SendPacket(Packet packet, bool reliable)
{
	enet_uint32 flag;
	if(reliable)
		flag = ENET_PACKET_FLAG_RELIABLE;
	else
		flag = ENET_PACKET_FLAG_UNSEQUENCED;
	ENetPacket* enetPacket = enet_packet_create(packet.GetData(),packet.GetDataSize(),flag);
	enet_peer_send(server,0,enetPacket);
}

bool Network::Connect(std::string ip)
{
	enet_address_set_host(&serverAddress, ip.c_str());
    serverAddress.port = NET_PORT;
	server = enet_host_connect(client, &serverAddress, 2, 0);
	ENetEvent event;
	if (enet_host_service(client, &event, CONNECTION_TIMEOUT) > 0 && event.type == ENET_EVENT_TYPE_CONNECT)
    {
		LogWrite("Connection to %s:%i succeeded!",ip.c_str(),NET_PORT);
		connected = true;
		LogWrite("Starting Network Update Loop");
		updateLoopRunning = true;
		workerThread = boost::thread(&Network::UpdateLoop,this);
		return true;
    }
    else
    {
        enet_peer_reset(server);
		LogWriteError("Connection to %s:%i failed!",ip.c_str(),NET_PORT);
		return false;
    }
}

void Network::Init(GameEngine* engine)
{
	this->engine = engine;
	enet_initialize();
	client = enet_host_create(NULL,1,2,57600,14400); // Start Client with 56kbps down and 14kps down
	verify(client != NULL,"Client != NULL",0);
}

void Network::UpdateLoop()
{
	while(updateLoopRunning)
	{
		ENetEvent event;
		int result = enet_host_service(client, &event, 1);
		if(result > 0)
		{
			if(event.type == ENET_EVENT_TYPE_RECEIVE)
				packetQueue.push(event.packet);
		}
	}
}

void Network::HandlePackets()
{
	if(!packetQueue.empty() && connected)
	{
		ENetPacket* enetPacket = packetQueue.front();
		Packet packet;
		packet.SetData((char*)enetPacket->data,enetPacket->dataLength);
		OnPacketIncomming(packet);
		enet_packet_destroy(enetPacket);
		packetQueue.pop();
	}
}

void Network::SetLocalPlayerID(Int32 id)
{
	localPlrID = id;
}

void Network::AddClientPlayer(Int32 id)
{
	LogWrite("Player %i joined the Game!",id);
	clientPlr[id] = ClientData(id,Vector2());
}

void Network::AddClientPlayer(ClientData data)
{
	LogWrite("Player %i joined the Game!",data.ID);
	clientPlr[data.ID] = data;
}

void Network::DeleteClientPlayer(Int32 id)
{
	LogWrite("Player %i leave the Game!",id);
	clientPlr.erase(clientPlr.begin()+id);
}