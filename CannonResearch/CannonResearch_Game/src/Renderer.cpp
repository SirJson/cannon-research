#include "Renderer.h"
#ifdef WIN32
	#include <Windows.h>
#endif
#include <GL/gl.h>
#include "Texture2D.h"
#include "Foreach.h"
#include "Camera.h"
#include "Sprite.h"
#include "Logger.h"

Renderer::Renderer(int width, int height)
{
    LogWrite("Init OpenGL Renderer...");
    glClearColor(0.5,0.5,0.5,0);
    glClearDepth(1.0);
    LogWrite("Enable Depth Test...");
    glEnable(GL_DEPTH_TEST);
    LogWrite("Enable Textures...");
    glEnable(GL_TEXTURE_2D);
    LogWrite("Enable Blending...");
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glViewport(0, 0, width, height);
    camera = new Camera(width,height);
}

Renderer::~Renderer()
{
    LogWrite("Destroy OpenGL Renderer...");
    foreach(render_list::value_type &renderItem, renderList)
    {
        delete renderItem.second;
    }
}

void Renderer::AddSprite(std::string name, Sprite* sprite)
{
    renderList[name] = sprite;
	renderList[name]->SetName(name);
}

void Renderer::Draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    foreach(render_list::value_type &renderItem, renderList)
    {    
		renderItem.second->Draw();
    }
}
