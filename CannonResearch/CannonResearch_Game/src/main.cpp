#ifdef __cplusplus
    #include <cstdlib>
#else
    #include <stdlib.h>
#endif
#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include "GameEngine.h"
#include "TestGameState.h"
#include "Logger.h"

int main(int argc, char** argv)
{
	GameEngine& engine = GameEngine::GetInstance();
	engine.Init();
	engine.SetGameState(new TestGameState());
	while(engine.IsRunning())
	{
		engine.Update();
	}
	engine.Exit();
	engine.Cleanup();
	GameEngine::DestroySingleton();
	// all is well ;)
	LogWrite("Exited cleanly");
	return 0;
}
