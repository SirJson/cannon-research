#include "GameEngine.h"

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include "GameState.h"
#include "Renderer.h"
#include "Logger.h"
#include "Utility.h"
#include "Network.h"

GameEngine* GameEngine::instance = 0;

void GameEngine::Init()
{
	InitLogger();
	LogWrite("Init GooEngine...");
	isExit = false;
	config.Load("game_config.cfg");
	LogWrite("Init SDL...");
	if (SDL_Init( SDL_INIT_VIDEO ) < 0)
		LogWriteError(SDL_GetError());
	screenWidth = config.GetIntValue("screenWidth");
	screenHeight = config.GetIntValue("screenHeight");
	fullscreen = config.GetBoolValue("fullscreen");
	gfxCore.init(screenWidth,screenHeight,fullscreen);
	inputMgr.Init(screenWidth,screenHeight,gfxCore.GetRenderer()->GetCamera());
	net.Init(this);
	gameState = NULL;
	InitRandom();
}

void GameEngine::Cleanup()
{
	LogWrite("Shutdown Engine...");
	SDL_Quit();
    if(gameState != NULL)
        gameState->Shutdown();
}

void GameEngine::SetGameState(GameState* state)
{
    gameState = state;
    gameState->Init(this);
}

void GameEngine::Update()
{
	net.HandlePackets();
	inputMgr.Update();
	if(gameState != NULL)
		gameState->Update();
	gfxCore.Draw();
	handleEvents();
}

void GameEngine::handleEvents()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_QUIT:
                isExit = true;
                break;
			case SDL_KEYDOWN:
				inputMgr.HandleKeyDown(event.key);
				break;
			case SDL_KEYUP:
				inputMgr.HandleKeyUp(event.key);
				break;
        }
    }
}

bool GameEngine::IsRunning()
{
    return !isExit;
}

void GameEngine::Exit()
{
	isExit = true;
}

void GameEngine::DestroySingleton()
{
	if (instance)
		delete instance;
	instance = 0;
}

GameEngine& GameEngine::GetInstance()
{
	if (!instance)
		instance = new GameEngine();
	return *instance;
}