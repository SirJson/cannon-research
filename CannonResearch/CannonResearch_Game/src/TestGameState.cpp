#include "TestGameState.h"
#include "GraphicsCore.h"
#include "GameEngine.h"
#include "Sprite.h"
#include "Texture2D.h"
#include "Landscape.h"
#include "InputManager.h"
#include "Renderer.h"
#include "Camera.h"
#include <boost/bind.hpp>
#include "Logger.h"
#include <Size2D.h>
#include "NetProtocol.h"
#include <iostream>

void TestGameState::Init(GameEngine* engine)
{
	this->engine = engine;
	level = new Landscape(3000,1000,*engine);
	engine->GetInputManager().BindKeyDown.connect(boost::bind(&TestGameState::HandleInput,this,_1));
	engine->GetNetworkManager().OnPacketIncomming.connect(boost::bind(&TestGameState::HandlePacket,this,_1));
	engine->GetInputManager().BindKey("CameraUp",SDLK_w);
	engine->GetInputManager().BindKey("CameraDown",SDLK_s);
	engine->GetInputManager().BindKey("CameraRight",SDLK_d);
	engine->GetInputManager().BindKey("CameraLeft",SDLK_a);
	std::string str = "localhost";
	bool result = false;
	if(str != "")
		result = engine->GetNetworkManager().Connect(str.c_str());
	else
		result = engine->GetNetworkManager().Connect("localhost");
	if(!result)
		level->Generate(30);
	player.Init();
	if(result == false)
		player.SetPosition(level->GetTop(300));
}

void TestGameState::HandleInput(std::string name)
{
	if(name == "CameraUp")
		engine->GetGraphicsCore().GetRenderer()->GetCamera()->AddPosition(Vector2(0,1));
	if(name == "CameraDown")
		engine->GetGraphicsCore().GetRenderer()->GetCamera()->AddPosition(Vector2(0,-1));
	if(name == "CameraRight")
		engine->GetGraphicsCore().GetRenderer()->GetCamera()->AddPosition(Vector2(1,0));
	if(name == "CameraLeft")
		engine->GetGraphicsCore().GetRenderer()->GetCamera()->AddPosition(Vector2(-1,0));
}

void TestGameState::HandlePacket(Packet packet)
{
	int id;
	packet >> id;
	if(id == ServerProtocol::CMD_LandscapeTransmission)
	{
		LogWrite("Getting CMD_LandscapeTransmission");
		std::vector<Int32> terrainContour;
		packet >> terrainContour;
		level->CreateFromContour(terrainContour);
	}
	if(id == ServerProtocol::CMD_RemoveCircleFromTerrain)
	{
		LogWrite("Getting CMD_RemoveCircleFromTerrain");
		float posX, posY;
		int radius;
		packet >> posX >> posY >> radius;
		level->RemoveCircleFromTerrain(Vector2(posX,posY),radius);
	}
	if(id == ServerProtocol::INFO_ConnectionData)
	{
		Int32 id;
		packet >> id;
		engine->GetNetworkManager().SetLocalPlayerID(id);
	}
	if(id == ServerProtocol::INFO_ClientConnected)
	{
		Int32 id;
		packet >> id;
		if(id != engine->GetNetworkManager().GetLocalPlayerID())
			engine->GetNetworkManager().AddClientPlayer(id);
	}
	if(id == ServerProtocol::INFO_ClientDisconnected)
	{
		Int32 id;
		packet >> id;
		if(id == engine->GetNetworkManager().GetLocalPlayerID())
			engine->GetNetworkManager().SetLocalPlayerID(-1);
		else
			engine->GetNetworkManager().DeleteClientPlayer(id);
	}
}

void TestGameState::Update()
{
	if(engine->GetInputManager().IsMousePressed(1))
	{
		LogWrite("Send REQ_RemoveCircleFromTerrain");
		Packet packet;
		packet << ServerProtocol::REQ_RemoveCircleFromTerrain << engine->GetInputManager().GetMousePos().X << engine->GetInputManager().GetMousePos().Y << 10;
		level->RemoveCircleFromTerrain(engine->GetInputManager().GetMousePos(),10);
		engine->GetNetworkManager().SendPacket(packet);
	}	
	//engine->GetGraphicsCore()->GetRenderer()->GetSprite("ViewRect")->SetPosition(engine->GetGraphicsCore()->GetRenderer()->GetCamera()->GetPosition());
	level->Render();
}