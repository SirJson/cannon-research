#include "Landscape.h"
#include "Texture2D.h"
#include "PNGImage.h"
#include <cmath>
#include "Logger.h"
#include "Utility.h"
#include <stdlib.h>
#include <stdio.h>
#include "GameEngine.h"
#include "Sprite.h"
#include "Vector2.h"
#include "GraphicsCore.h"
#include "Foreach.h"
#include "Renderer.h"
#include "Camera.h"

Landscape::Landscape(int width, int height, GameEngine& engine, std::string skyTextureFile) : engine(engine)
{
    this->width = width;
    this->height = height;
    // 2-Dimensionales Array auf 1-Dimensionales abbilden
    groundTexture = new PNGImage();
	skyTexture = new PNGImage();
    if(!groundTexture->Load("ground.png"))
        LogWriteError("Could not load ground.png!");
	if(!skyTexture->Load(skyTextureFile.c_str()))
        LogWriteError("Could not load ground.png!");
}

Landscape::~Landscape()
{
	//delete [] chunks;
	delete skyTexture;
	delete groundTexture;
	delete landscapeImage;
}

void Landscape::Generate(float peakheight, float flatness)
{
	contourGen = ContourGenerator(width,height,peakheight,flatness);
	this->terrainContour = contourGen.GetTerrainContour();
    CreateForeground();
}

Vector2 Landscape::GetTop(int x)
{
	return Vector2(x,terrainContour[x]);
}

void Landscape::Create(Image image)
{
	landscapeImage = &image;
	MakeChunks(256);
}

void Landscape::CreateFromContour(std::vector<int> terrainContour)
{
	this->terrainContour = terrainContour;
	CreateForeground();
}

void Landscape::CreateForeground()
{
	landscapeImage = new Image(4,width,height);
	
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			if (y < terrainContour[x])
				landscapeImage->At(x,y) = groundTexture->At(x % groundTexture->GetWidth(), y % groundTexture->GetHeight());
			else
				landscapeImage->At(x,y) = skyTexture->At(x % skyTexture->GetWidth(), y % skyTexture->GetHeight());
				//landscapeImage->At(x,y) = Color(0,0,0,0);
		}
	}
 	MakeChunks(256);
}

Color& Landscape::At(int x, int y)
{
	Sprite* sprite = GetChunk(Vector2(x,y));
	if(sprite == NULL)
		return landscapeImage->At(0,0);
	return sprite->GetTexture()->GetImage()->At(x % chunkSize, y % chunkSize);
}

void Landscape::RemoveCircleFromTerrain(Vector2 Position, int radius)
{
	for (int y = -radius; y <= radius; y++)
	{
		for (int x = -radius; x <= radius; x++)
		{
			if (x * x + y * y <= radius * radius)
			{
				At(Position.X+x, Position.Y+y) = Color(0,0,0,0);
				Sprite* sprite = GetChunk(Vector2(Position.X+x, Position.Y+y));
				if(sprite != NULL)
					sprite->GetTexture()->UpdateInfo();
			}
		}
	}
}

Sprite* Landscape::GetChunk(Vector2 vec)
{
	foreach(Sprite* sprite,chunks)
	{
		if(sprite->GetBounds().Contains(vec))
			return sprite;
	}
	return NULL;
}

void Landscape::Render()
{
	foreach(Sprite* sprite,chunks)
	{
		if(engine.GetGraphicsCore().GetRenderer()->GetCamera()->GetViewRectangle().Intersects(sprite->GetBounds()))
		{
			if(sprite->GetActive() != true)
			{
				sprite->SetActive(true);
			}
		}
		else
		{
			if(sprite->GetActive() != false)
			{
				sprite->SetActive(false);
			}
		}
	}
}

void Landscape::MakeChunks(int chunkSize)
{
	this->chunkSize = chunkSize;
	int chunkCountX = width / chunkSize;
	chunkCountY = height / chunkSize;
	
	if(width % chunkSize)
		chunkCountX++;
	if(height % chunkSize)
		chunkCountY++;
	
	chunksCount = chunkCountX * chunkCountY;
	int count = 0;
	chunks.resize(chunksCount);
	for (int x = 0; x < chunkCountX; x++)
	{
		for (int y = 0; y < chunkCountY; y++)
		{
			Image* image = new Image(*landscapeImage,Rectangle2D(chunkSize*x,chunkSize*y,chunkSize,chunkSize)); //Color2DArray(SplitArraySequential(landscapeData->GetPlainData(),(chunkSize * x) + width * (chunkSize * y),chunkSize+width*chunkSize),chunkSize,chunkSize);
			std::string chunkName = "chunk" + ValToString<int>(count);
			chunks[count] = new Sprite(new Texture2D(image));
			chunks[count]->SetPosition(Vector2(chunkSize * x,chunkSize * y));
			chunks[count]->SetActive(false);
			engine.GetGraphicsCore().AddSprite(chunkName,chunks[count]);
			count++;
		}
	}
	LogWrite("Chunk Generation finished!");
}
