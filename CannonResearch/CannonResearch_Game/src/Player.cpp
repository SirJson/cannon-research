#include "Player.h"
#include "Texture2D.h"
#include "Vector2.h"
#include "GameEngine.h"

Player::Player()
{
	Speed = 5;
}

Player::~Player()
{
}

void Player::Init()
{
	figure = new Sprite(new Texture2D("soldier.png"),Vector2(0,0));
	GameEngine::GetInstance().GetGraphicsCore().AddSprite("soldier",figure);
}

void Player::SetPosition(Vector2 Position)
{
	figure->SetPosition(Position);
}