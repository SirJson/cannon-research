#include "Camera.h"
#ifdef WIN32
	#include <Windows.h>
#endif
#include <GL/gl.h>
#include "Vector2.h"
#include "Rectangle2D.h"

Camera::Camera(int screenWidth, int screenHeight)
{
	pos = Vector2(0,0);
	viewRect = Rectangle2D(0, 0, screenWidth, screenHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,800,0,600,0,128);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

Camera::~Camera()
{
}

void Camera::applyCameraChange()
{
	viewRect.X = pos.X;
	viewRect.Y = pos.Y;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-pos.X,-pos.Y,0);
}

void Camera::SetPosition(Vector2 val)
{
	pos = val;
	applyCameraChange();
}

void Camera::AddPosition(Vector2 val)
{
	pos += val;
	applyCameraChange();
}

void Camera::DecreasePosition(Vector2 val)
{
	pos -= val;
	applyCameraChange();
}

Vector2 Camera::GetPosition()
{
	return pos;
}

Rectangle2D Camera::GetViewRectangle()
{
	return viewRect;
}
