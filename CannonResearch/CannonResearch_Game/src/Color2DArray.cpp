#include "Color2DArray.h"
#include "Texture2D.h"
#include "Image.h"

Color2DArray::Color2DArray()
{
	this->width = 0;
    this->height = 0;
    this->data = NULL;
}

Color2DArray::Color2DArray(int width, int height)
{
    this->width = width;
    this->height = height;
    this->data = new Color[width * height];
}

Color2DArray::Color2DArray(Color* data, int width, int height)
{
    this->width = width;
    this->height = height;
    this->data = data;
}

Color2DArray::~Color2DArray()
{
    delete [] data;
}

void Color2DArray::SetPlainData(Color* data)
{
	this->data = data;
}

void Color2DArray::ReallocateMemory(int width, int height)
{
	this->width = width;
    this->height = height;
	this->data = new Color[width * height];
}