#include "GraphicsCore.h"
#include "Renderer.h"
#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif
#include "Logger.h"

GraphicsCore::GraphicsCore()
{
}

void GraphicsCore::init(int screenWidth, int screenHeight, bool fullscreen)
{
	this->screenWidth = screenWidth;
	this->screenHeight = screenHeight;
	LogWrite("Init Graphics Core...");
	LogWrite("Create SDL Surface...");
	const SDL_VideoInfo* info = SDL_GetVideoInfo();
	if(info == NULL)
		LogWriteError("Unable to get video info!");
	int videoFlags = SDL_OPENGL | SDL_DOUBLEBUF | SDL_HWPALETTE;
	if(info->hw_available != 0)
		videoFlags = videoFlags | SDL_HWSURFACE;
	else
		videoFlags = videoFlags | SDL_SWSURFACE;
	if(info->blit_hw != 0)
		videoFlags = videoFlags | SDL_HWACCEL;
	if(fullscreen)
		videoFlags = videoFlags | SDL_FULLSCREEN;
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_WM_SetCaption("Cannon Research", NULL);
	screen = SDL_SetVideoMode(screenWidth, screenHeight, 32, videoFlags);
	renderer = new Renderer(screenWidth,screenHeight);
}

GraphicsCore::~GraphicsCore()
{
    LogWrite("Destroy Graphics Core...");
    delete renderer;
}

void GraphicsCore::AddSprite(std::string name, Sprite* sprite)
{
    renderer->AddSprite(name,sprite);
}

void GraphicsCore::Draw()
{
    renderer->Draw();
    SDL_GL_SwapBuffers();
}
