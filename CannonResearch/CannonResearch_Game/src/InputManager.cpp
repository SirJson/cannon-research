#include "InputManager.h"
#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif
#include "Vector2.h"
#include "Foreach.h"
#include "Logger.h"
#include "Camera.h"

InputManager::InputManager()
{
	keyState.reset();
	screenWidth = 0;
	screenHeight = 0;
}

InputManager::~InputManager(void)
{
}

void InputManager::Init(int width, int height, Camera* camera)
{
	LogWrite("Init Input Manager...");
	screenWidth = width;
	screenHeight = height;
	cam = camera;
}

bool InputManager::IsKeyDown(SDLKey key)
{
	if(keyState[key])
		return true;
	else
		return false;
}

bool InputManager::IsKeyUp(SDLKey key)
{
	if(!keyState[key])
		return true;
	else
		return false;
}

bool InputManager::IsKeyPressed(SDLKey key)
{
	if(keyState[key])
		return true;
	else
		return false;
}

bool InputManager::IsMouseDown(int button)
{
	if(mouseState & SDL_BUTTON(SDL_BUTTON_LEFT))
		return true;
	else
		return false;
}

bool InputManager::IsMouseUp(int button)
{
	if(!(mouseState & SDL_BUTTON(SDL_BUTTON_LEFT)))
		return true;
	else
		return false;
}

bool InputManager::IsMousePressed(int button)
{
	if((mouseState & SDL_BUTTON(SDL_BUTTON_LEFT)) && !(prevMouseState & SDL_BUTTON(SDL_BUTTON_LEFT)))
		return true;
	else
		return false;
}

bool InputManager::IsKeyDown(std::string bindingName)
{
	return IsKeyDown(keyMapping[bindingName]);
}

bool InputManager::IsKeyUp(std::string bindingName)
{
	return IsKeyUp(keyMapping[bindingName]);
}

bool InputManager::IsKeyPressed(std::string bindingName)
{
	return IsKeyPressed(keyMapping[bindingName]);
}

SDLKey InputManager::GetBinding(std::string name)
{
	return keyMapping[name];
}

Vector2 InputManager::GetMousePos()
{
	Vector2 vector(0,0);
	int x,y;
	SDL_GetMouseState(&x,&y);
	vector.X = x;
	vector.Y = screenHeight - y;
	vector += cam->GetPosition();
	return vector;
}

Vector2 InputManager::GetRealMousePos()
{
	Vector2 vector(0,0);
	int x,y;
	SDL_GetMouseState(&x,&y);
	vector.X = x;
	vector.Y = screenHeight - y;
	return vector;
}


Vector2 InputManager::GetRelativeMousePos()
{
	Vector2 vector(0,0);
	int x,y;
	SDL_GetMouseState(&x,&y);
	vector.X = x;
	vector.Y = screenHeight - y; // Immo nur zu Testzwecken statisch weil wir mit 800x600 starten
	return vector;
}

void InputManager::BindKey(std::string name, SDLKey key)
{
	keyMapping[name] = key;
}

void InputManager::HandleKeyDown(SDL_KeyboardEvent event)
{
	foreach(key_mapping::value_type binding, keyMapping)
	{
		if(binding.second == event.keysym.sym)
			BindKeyDown(binding.first);
	}
	keyState[event.keysym.sym] = true;
}

void InputManager::HandleKeyUp(SDL_KeyboardEvent event)
{
	foreach(key_mapping::value_type binding, keyMapping)
	{
		if(binding.second == event.keysym.sym)
			BindKeyPressed(binding.first);
	}
	keyState[event.keysym.sym] = false;
}

void InputManager::Update()
{
	prevMouseState = mouseState;
	mouseState = SDL_GetMouseState(NULL,NULL);
	foreach(key_mapping::value_type binding, keyMapping)
	{
		if(keyState[binding.second] == true)
			BindKeyDown(binding.first);
	}
}