﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CannonResearch
{
	class Landscape
	{
		public int[] TerrainData, TerrainData2;
		int screenWidth, screenHeight;
		private Texture2D _landscape = null;
		private Texture2D groundTexture;
		private Random _randomizer = new Random();
		public Color[,] LandscapeColorArray;

		public void Init(Game game, int worldWidth, int worldHeight)
		{
			screenWidth = worldWidth;
			screenHeight = worldHeight;
			groundTexture = game.Content.Load<Texture2D>("earth");
		}

		public void Generate(GraphicsDevice device)
		{
			GenerateTerrainContour();
			CreateForeground(device);
		}

		public void UpdateTerrain(GraphicsDevice device)
		{
			CreateForeground(device);
		}

		/*
		public void AddCrater(Color[,] tex, Matrix mat)
		{
			int width = tex.GetLength(0);
			int height = tex.GetLength(1);

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					if (tex[x, y].R > 10)
					{
						Vector2 imagePos = new Vector2(x, y);
						Vector2 screenPos = Vector2.Transform(imagePos, mat);

						int screenX = (int)screenPos.X;
						int screenY = (int)screenPos.Y;

						if ((screenX) > 0 && (screenX < screenWidth))
							if (TerrainData[screenX] < screenY)
								TerrainData[screenX] = screenY;
					}
				}
			}
		}
		 */

		private void RemoveHorizontalLineFromTerrain(int x1, int x2, int y)
		{
			for (int i = x1; i != x2; i++)
			{
				LandscapeColorArray[x1 + i, y] = Color.Transparent;
			}
		}

		private void RemoveCircleFromTerrain(Vector2 Position, int radius)
		{
			for (int y = -radius; y <= radius; y++)
				for (int x = -radius; x <= radius; x++)
					if (x * x + y * y <= radius * radius)
						if (Position.X + x <= LandscapeColorArray.GetLength(0) && (int)Position.Y + y <= LandscapeColorArray.GetLength(1))
							LandscapeColorArray[(int)Position.X + x, (int)Position.Y + y] = Color.Transparent;
		}

		public void AddCrater(Vector2 Position, int radius)
		{
			for (int i = 0; i != radius; i++)
				RemoveCircleFromTerrain(Position, i);

			// Update with new Data
			Color[] foregroundColors = new Color[screenWidth * screenHeight];
			for (int x_i = 0; x_i < screenWidth; x_i++)
			{
				for (int y_i = 0; y_i < screenHeight; y_i++)
				{
					if (y_i > TerrainData[x_i])
						foregroundColors[x_i + y_i * screenWidth] = LandscapeColorArray[x_i, y_i];
					else
						foregroundColors[x_i + y_i * screenWidth] = Color.Transparent;
				}
			}
			_landscape.SetData(foregroundColors);
		}

		public Color[,] TextureTo2DArray(Texture2D texture)
		{
			Color[] colors1D = new Color[texture.Width * texture.Height];
			texture.GetData(colors1D);

			Color[,] colors2D = new Color[texture.Width, texture.Height];
			for (int x = 0; x < texture.Width; x++)
				for (int y = 0; y < texture.Height; y++)
					colors2D[x, y] = colors1D[x + y * texture.Width];

			return colors2D;
		}

		public Vector2 TexturesCollide(Color[,] tex1, Matrix mat1, Color[,] tex2, Matrix mat2)
		{
			Matrix mat1to2 = mat1 * Matrix.Invert(mat2);
			int width1 = tex1.GetLength(0);
			int height1 = tex1.GetLength(1);
			int width2 = tex2.GetLength(0);
			int height2 = tex2.GetLength(1);

			for (int x1 = 0; x1 < width1; x1++)
			{
				for (int y1 = 0; y1 < height1; y1++)
				{
					Vector2 pos1 = new Vector2(x1, y1);
					Vector2 pos2 = Vector2.Transform(pos1, mat1to2);

					int x2 = (int)pos2.X;
					int y2 = (int)pos2.Y;
					if ((x2 >= 0) && (x2 < width2))
					{
						if ((y2 >= 0) && (y2 < height2))
						{
							if (tex1[x1, y1].A > 0)
							{
								if (tex2[x2, y2].A > 0)
								{
									Vector2 screenPos = Vector2.Transform(pos1, mat1);
									return screenPos;
								}
							}
						}
					}
				}
			}

			return new Vector2(-1, -1);
		}

		private void GenerateTerrainContour()
		{
			TerrainData = new int[screenWidth];
			TerrainData2 = new int[screenHeight];

			double rand1 = _randomizer.NextDouble() + 1;
			double rand2 = _randomizer.NextDouble() + 2;
			double rand3 = _randomizer.NextDouble() + 3;

			float offset = screenHeight / 2;
			float peakheight = 100;
			float flatness = 70;

			for (int x = 0; x < screenWidth; x++)
			{
				double height = peakheight / rand1 * Math.Sin((float)x / flatness * rand1 + rand1);
				height += peakheight / rand2 * Math.Sin((float)x / flatness * rand2 + rand2);
				height += peakheight / rand3 * Math.Sin((float)x / flatness * rand3 + rand3);
				height += offset;
				TerrainData[x] = (int)height;
			}
		}

		private void CreateForeground(GraphicsDevice device)
		{
			Color[,] groundColors = TextureTo2DArray(groundTexture);
			Color[] foregroundColors = new Color[screenWidth * screenHeight];

			for (int x = 0; x < screenWidth; x++)
			{
				for (int y = 0; y < screenHeight; y++)
				{
					if (y > TerrainData[x])
						foregroundColors[x + y * screenWidth] = groundColors[x % groundTexture.Width, y % groundTexture.Height];
					else
						foregroundColors[x + y * screenWidth] = Color.Transparent;
				}
			}
			_landscape = new Texture2D(device, screenWidth, screenHeight, false, SurfaceFormat.Color);
			_landscape.SetData(foregroundColors);
			LandscapeColorArray = TextureTo2DArray(_landscape);
		}

		public void Draw(SpriteBatch batch)
		{
			batch.Draw(_landscape, new Vector2(0, 0), Color.White);
		}
	}
}
