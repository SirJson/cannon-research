SET(SHORTNAME "cnr")
SET(RootDir "${CMAKE_SOURCE_DIR}")
SET(SourceDir "${RootDir}/CannonResearch_Game/src")
SET(TargetDir "${RootDir}/CannonResearch_Game/bin/release")
SET(IncludeDir "${RootDir}/CannonResearch_Game/include")
SET(SharedIncDir "${RootDir}/CannonResearch_Shared/include")
SET(CMakeDir "${RootDir}/CannonResearch_Game/cmake")

SET(CMAKE_MODULE_PATH ${CMakeDir})

if(WIN32)
	SET(LibIncludeDir "D:/Dev/Libs/SDL/include" "D:/Dev/Libs/lpng141" "D:/Dev/Libs/zlib" "D:/Dev/Libs/enet-1.3.3/include")
	SET(LibDirs "D:/Dev/Libs/enet-1.3.3/Debug" "D:/Dev/Libs/SDL/lib" "D:/Dev/Libs/lpng141/projects/visualc71/Win32_LIB_Release" "${RootDir}/CannonResearch_Shared")
	SET(Libs "opengl32.lib" "libpng.lib" "SDLmain.lib" "SDL.lib" "enet.lib" "zlib.lib" "ws2_32.lib" "winmm.lib")
else(WIN32)
	SET(LibIncludeDir "/usr/include")
	SET(LibDirs "/usr/lib" "/usr/lib/x86_64-linux-gnu/" "${RootDir}/CannonResearch_Shared")
	SET(Libs "libGL.so" "libpng.so" "libSDL.so" "enet.so" "libboost_signals.so" "libboost_thread.so")
endif(WIN32)
SET(IncDirs ${IncludeDir} ${LibIncludeDir} ${SharedIncDir})

FILE(GLOB_RECURSE Header "${IncludeDir}/*.h") # Benoetigt wegen MSVC so don't flame!
FILE(GLOB_RECURSE Sources "${SourceDir}/*.?pp")

SET(CMAKE_BUILD_TYPE Debug)

LINK_DIRECTORIES(${LibDirs})
INCLUDE_DIRECTORIES(${IncDirs})
ADD_EXECUTABLE(${SHORTNAME} ${Sources} ${Header})
TARGET_LINK_LIBRARIES(${SHORTNAME} ${Libs} "cnrshared")
