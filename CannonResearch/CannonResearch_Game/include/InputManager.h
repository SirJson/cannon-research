#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <map>
#include <string>
#include <bitset>
#include <boost/signals.hpp>
#include "stdint.h"
#ifdef WIN32
#include <SDL_keysym.h>
#else
#include <SDL/SDL_keysym.h>
#endif

struct SDL_KeyboardEvent;
struct SDL_MouseMotionEvent;
struct SDL_MouseButtonEvent;
struct Vector2;

class Camera;

typedef std::map<std::string,SDLKey> key_mapping;
typedef uint8_t	Uint8;

class InputManager
{
public:
	InputManager();
	~InputManager(void);
	void Init(int width, int height, Camera* camera);
	bool IsKeyDown(SDLKey key);
	bool IsKeyUp(SDLKey key);
	bool IsKeyPressed(SDLKey key);
	bool IsKeyDown(std::string bindingName);
	bool IsKeyUp(std::string bindingName);
	bool IsKeyPressed(std::string bindingName);
	bool IsMouseDown(int button);
	bool IsMouseUp(int button);
	bool IsMousePressed(int button);
	void BindKey(std::string name, SDLKey key);
	SDLKey GetBinding(std::string name);
	Vector2 GetMousePos();
	Vector2 GetRealMousePos();
	Vector2 GetRelativeMousePos();
	void Update();
	void HandleKeyDown(SDL_KeyboardEvent event);
	void HandleKeyUp(SDL_KeyboardEvent event);
	boost::signal<void (std::string)> BindKeyDown;
	boost::signal<void (std::string)> BindKeyPressed;
private:
	Camera* cam;
	std::map<std::string,SDLKey> keyMapping;
	std::bitset<SDLK_LAST> keyState;
	Uint8 mouseState;
	Uint8 prevMouseState;
	int screenWidth, screenHeight;
};

#endif // INPUTMANAGER_H

