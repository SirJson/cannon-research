#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include "GraphicsCore.h"
#include "InputManager.h"
#include "Network.h"
#include "Config.h"

class GameState;

class GameEngine
{
    public:
		static GameEngine& GetInstance();
		static void DestroySingleton();
		void Exit();
		void Cleanup();
        void Init();
        void Update();
        GraphicsCore& GetGraphicsCore() { return gfxCore; }
        InputManager& GetInputManager() { return inputMgr; }
        Network& GetNetworkManager() { return net; }
		Config& GetConfig() { return config; }
        void SetGameState(GameState* state);
		bool IsRunning();
    protected:
    private:
		GameEngine() {}
		GameEngine(const GameEngine&) {}
        ~GameEngine() {}
		static GameEngine* instance;
        void handleEvents();
        GraphicsCore gfxCore;
		InputManager inputMgr;
		Network net;
        bool isExit;
        GameState* gameState;
		Config config;
		int screenWidth, screenHeight;
		bool fullscreen;
};

#endif // GAMEENGINE_H
