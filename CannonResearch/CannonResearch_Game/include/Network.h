#ifndef NETWORK_H
#define NETWORK_H

#include <enet/enet.h>
#include <string>
#include "Packet.h"
#include <boost/signals.hpp>
#include <boost/thread.hpp>
#include <queue>
#include "ClientData.h"

class GameEngine;

class Network
{
    public:
		Network();
		~Network();
		void Init(GameEngine* engine);
		bool Connect(std::string ip);
		void SendPacket(Packet packet, bool reliable = true);
		void HandlePackets();

		/**
		 * Setzt die Lokale Netzwerk Spieler ID
		 */
		void SetLocalPlayerID(Int32 id);
		
		/**
		 * Informiert diesen Client �ber einen anderen verbundenen
		 */
		void AddClientPlayer(Int32 id);
		void AddClientPlayer(ClientData data);

		/**
		 * Informiert diesen Client das ein anderer das Spiel verlassen hat
		 */
		void DeleteClientPlayer(Int32 id);

		/**
		 * Gibt die Lokale Netzwerk Spieler ID zur�ck
		 */
		Int32 GetLocalPlayerID() const { return localPlrID; }

		/**
		 * Gibt die Client Daten eines Clienten zur�ck
		 */
		ClientData GetClientPlayer(Int32 ID) const { return clientPlr[ID]; }

		boost::signal<void (Packet)> OnPacketIncomming;
    protected:
    private:
		void UpdateLoop();

		ENetHost* client;
		ENetPeer* server;
		ENetAddress serverAddress;
		GameEngine* engine;
		boost::thread workerThread;

		std::queue<ENetPacket*> packetQueue;
		bool connected;
		bool updateLoopRunning;

		Int32 localPlrID;
		std::vector<ClientData> clientPlr;
};

#endif // NETWORK_H
