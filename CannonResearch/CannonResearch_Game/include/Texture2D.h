#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <string>
class Image;

class Texture2D
{
    public:
		Texture2D();
		Texture2D(Image* image, bool useMipMaps = false, bool niceFiltering = false);
        Texture2D(unsigned char* data, int width, int height, int channels, bool linearFilter = false);
        Texture2D(std::string file, bool useMipMaps = false);
        virtual ~Texture2D();
        int GetHandle() { return handle; }
        int GetWidth() { return width; }
        int GetHeight() { return height; }
        Image* GetImage() { return image; }
        void UpdateInfo();
    protected:
    private:
		void MakeChunks();
        int width;
        int height;
        unsigned int handle;
        unsigned int pboHandle;
        Image* image;
};

#endif // TEXTURE2D_H
