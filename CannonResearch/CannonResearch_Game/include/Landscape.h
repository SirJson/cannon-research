#ifndef LANDSCAPE_H
#define LANDSCAPE_H
#include "Color.h"
#include "Image.h"
#include <vector>
#include <string>
#include "DataTypes.h"
#include "ContourGenerator.h"

class GameEngine;
class Sprite;

using namespace std;

class Landscape
{
    public:
	Landscape(int width, int height, GameEngine& engine, std::string skyTextureFile = "Clouds2.png");
	virtual ~Landscape();

	int GetWidth() const { return width; }
	int GetHeight() const { return height; }
	Image* GetLandscapeImage() const { return landscapeImage; }
	Image* GetGroundTexture() const { return groundTexture; }
	void Generate(float peakheight = 100, float flatness = 140);
	void Create(Image image);
	void CreateFromContour(std::vector<Int32> terrainContour);
	Sprite* GetChunk(Vector2 pos);
	void RemoveCircleFromTerrain(Vector2 Position, int radius);
	void Render();
	Vector2 GetTop(int x);
	Color& At(int x, int y);

    private:
	void CreateForeground();
	void GenerateTerrainContour(float peakheight, float flatness);
	void MakeChunks(int chunkSize);

	int chunkSize;
	GameEngine& engine;
	std::vector<Int32> terrainContour;
	Image* landscapeImage;
	std::vector<Sprite*> chunks;
	int chunksCount, chunkCountY;
	int width, height;
	Image* groundTexture;
	Image* skyTexture;
	ContourGenerator contourGen;
};

#endif // LANDSCAPE_H
