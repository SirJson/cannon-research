#ifndef CAMERA_H
#define CAMERA_H

#include "Rectangle2D.h"
#include "Vector2.h"

class Camera
{
    public:
        Camera(int screenWidth, int screenHeight);
        virtual ~Camera();
        void SetPosition(Vector2 val);
        void AddPosition(Vector2 val);
        void DecreasePosition(Vector2 val);
		Vector2 GetPosition();
		Rectangle2D GetViewRectangle();
    protected:
    private:
        void applyCameraChange();
        Vector2 pos;
	Rectangle2D viewRect;
};

#endif // CAMERA_H
