#ifndef TESTGAMESTATE_H
#define TESTGAMESTATE_H

#include "GameState.h"
#include <string>
#include "Packet.h"
#include "Player.h"

class Landscape;

class TestGameState : public GameState
{
    public:
        void Init(GameEngine* engine);
		void Update();
		void HandleInput(std::string name);
		void HandlePacket(Packet packet);
    protected:
    private:
	    GameEngine* engine;
		Landscape* level;
		Player player;
};

#endif // TESTGAMESTATE_H
