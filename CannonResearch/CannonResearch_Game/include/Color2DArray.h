#ifndef IMAGEDATAARRAY_H
#define IMAGEDATAARRAY_H

#include "Color.h"

class Color2DArray
{
	public:
		Color2DArray();
		Color2DArray(int width, int height);
		Color2DArray(Color* data, int width, int height);
		virtual ~Color2DArray();
		Color* GetPlainData() { return data; }
		void ReallocateMemory(int width, int height);
		void SetPlainData(Color* data);
		int GetWidth() { return width; }
		int GetHeight() { return height; }
		Color* operator[](int Zeile)
		{
			// Array liegt "zeilenweise" im Speicher
			return & data[Zeile * height];
		}
	protected:
	private:
		Color* data;
		int width,height;
};

#endif // IMAGEDATAARRAY_H
