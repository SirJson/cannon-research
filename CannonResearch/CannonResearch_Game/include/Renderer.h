#ifndef RENDERER_H
#define RENDERER_H

#include <map>
#include <string>
class Camera;
class Sprite;

typedef std::map<std::string,Sprite*> render_list;

class Renderer
{
    public:
        Renderer(int width, int height);
        virtual ~Renderer();
        void Draw();
        void AddSprite(std::string name, Sprite* sprite);
		Sprite* GetSprite(std::string name) { return renderList[name]; }
		Camera* GetCamera() { return camera; }
    protected:
    private:
        Camera* camera;
        render_list renderList;
};

#endif // RENDERER_H
