#ifndef GAMESTATE_H
#define GAMESTATE_H

class GameEngine;

class GameState
{
    public:
        virtual void Init(GameEngine* engine) = 0;
        virtual void Shutdown();
        virtual void Update();
    protected:
    private:
};

#endif // GAMESTATE_H
