#ifndef PLAYER_H
#define PLAYER_H

#include "Sprite.h"

class Player
{
public:
	Player();
	~Player();
	void Init();
	void SetPosition(Vector2 Position);
private:
	Sprite* figure;
	int Speed;
};

#endif // PLAYER_H