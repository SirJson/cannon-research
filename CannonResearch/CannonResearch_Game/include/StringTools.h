#ifndef TOOLS_STRING_H
#define TOOLS_STRING_H

#include <cctype>
#include <string>

namespace tool
{
namespace sz
{
	// C-Strings - Deprecated. Use the C++-Strings instead.
	bool SEqual(const char* A, const char* B, int iMaxLength = 128, bool bIgnoreCase = false);
	bool WildCardMatch(const char* Pattern, const char* Compare);
	bool WildCardMatch(char* Pattern, char* Compare);
    char* StringToChar(std::string string);

	// C++-Strings
	typedef int typeFn(int);
	extern typeFn* isalnum;
	extern typeFn* isalpha;
	extern typeFn* iscntrl;
	extern typeFn* isdigit;
	extern typeFn* isgraph;
	extern typeFn* islower;
	extern typeFn* isprint;
	extern typeFn* ispunct;
	extern typeFn* isspace;
	extern typeFn* isupper;
	extern typeFn* isxdigit;

	bool contains(std::string& String, std::string& Chars);
	bool containsFn(std::string& String,  size_t Count, ...);
	bool containsFnV(std::string& String, std::size_t Count, typeFn** FnV);

	std::size_t findFirstOfFn(std::string String, std::size_t Pos, size_t Count, ...);
	std::size_t findFirstOfFnV(std::string String, std::size_t Pos, size_t Count, typeFn** FnV);
	std::size_t findFirstNotOfFn(std::string String, std::size_t Pos, size_t Count, ...);
	std::size_t findFirstNotOfFnV(std::string String, std::size_t Pos, size_t Count, typeFn** FnV);

	void replaceAll(std::string& String, const std::string& Find, const std::string& Replace);
	void replaceAllChars(std::string& String, const std::string& Find, const std::string& Replace);
	void replaceAllChars(std::string& String, const std::string& Find, const char& Replace);
	void replaceAllChars(std::string& String, const char& Find, const char& Replace);

	void removeAll(std::string& String, const std::string& Find);
	void removeAllChars(std::string& String, const std::string& Find);
	void trim(std::string& String, const std::string& Chars);
	std::size_t split(const std::string& In, std::string& Out, const std::size_t& iPos, const char& iDelim);

	bool toBool(const std::string& String);
	int toInt(const std::string& String);
	float toFloat(const std::string& String);

	void toUpper(std::string& String);
	void toLower(std::string& String);


	static const char cHexLookUp[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

	inline std::string bin2Hex(const void* source, const std::size_t& length)
	{
		std::string out(length*2, 0);
		for(std::size_t i = 0; i < length ; i++)
		{
			out[i*2+1] = cHexLookUp[ (unsigned char)(((unsigned char*)source)[length-1-i] << 4) >> 4 ]; // 0000 XXXX
			out[i*2] = cHexLookUp[ ((unsigned char*)source)[length-1-i] >> 4 ]; // XXXX 0000
		}
		return out;
	}

	inline void hex2Bin(const std::string& source, void* target)
	{
		unsigned char x, y;
		for(std::size_t i = 0; i < source.size()/2; i++)
		{
			x = toupper(source[i*2+1]); y = toupper(source[i*2+0]);
			((char*)target)[i] = (unsigned char)(((x-((x >= 58)?(48+7):(48))) << 4) | (y-((y >= 58)?(48+7):(48))));
		}
	}
}
}

#endif
