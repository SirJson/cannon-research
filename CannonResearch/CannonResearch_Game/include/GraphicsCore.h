#ifndef GRAPHICSCORE_H
#define GRAPHICSCORE_H

class Renderer;
struct SDL_Surface;
class Sprite;

#include <string>

class GraphicsCore
{
    public:
        GraphicsCore();
        virtual ~GraphicsCore();
        void init(int screenWidth, int screenHeight, bool fullscreen);
        void Draw();
        void AddSprite(std::string name, Sprite* sprite);
        Renderer* GetRenderer() { return renderer; }
    protected:
    private:
        SDL_Surface* screen;
        Renderer* renderer;
	int screenWidth,screenHeight;
};

#endif // GRAPHICSCORE_H
