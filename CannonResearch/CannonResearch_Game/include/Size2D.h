#ifndef SIZE2D_H
#define SIZE2D_H

struct Size2D
{
	Size2D()
	{
		Width = 0;
		Height = 0;
	}

    Size2D(int width, int height)
    {
        Width = width;
        Height = height;
    }

    int Width;
    int Height;
};

#endif // SIZE2D_H
