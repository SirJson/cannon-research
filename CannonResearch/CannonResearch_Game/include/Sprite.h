#ifndef SPRITE_H
#define SPRITE_H

#include "Vector2.h"
#include "Rectangle2D.h"
#include <string>
#include "Size2D.h"
class Texture2D;

class Sprite
{
    public:
		Sprite();
		Sprite(Texture2D* texture);
        Sprite(Texture2D* texture,Vector2 pos);
        virtual ~Sprite();
        unsigned int GetWidth() { return width; }
        unsigned int GetHeight() { return height; }
        void Draw();
        void SetPosition(Vector2 pos);
        Size2D GetSize();
        Vector2 GetPosition();
		Rectangle2D GetBounds() { return _bounds; }
		void SetActive(bool mode);
		void SetSize(Size2D size);
		bool GetActive() { return enabled; }
		Texture2D* GetTexture() { return _tex; }
		void SetName(std::string spriteName) { name = spriteName; }
		std::string GetName() { return name; }
    protected:
    private:
        unsigned int width;
        unsigned int height;
		bool enabled;
        Vector2 _pos;
		Rectangle2D _bounds;
        Size2D _size;
        Texture2D* _tex;
		std::string name;
};

#endif // SPRITE_H
