/*************************
 * Cannon Research Server
 *************************/

#include "ServerMain.h"
#include "Logger.h"
#include "Input.h"
#include <cstdio>
#include "SrvNetwork.h"
#include "NetProtocol.h"

ServerMain::ServerMain()
{
	LogWrite("Init Server...");
	LogWrite("Init Network...");
	config.Load("srv_config.cfg");
	net.Init(this);
	landscape = new ContourGenerator(3000,1000,100,140);
}

ServerMain::~ServerMain()
{
	delete landscape;
}

/**
 * \brief Prüft ob der Server läuft
 * Prüft ob der Server läuft. Gibt false zurück wenn er nicht läuft ;)
 */
bool ServerMain::IsRunning()
{
	return net.IsRunning();
}

/**
 * \brief Updatet den Server
 */
void ServerMain::Update()
{
	if(kbhit())
	{
		char ch = getch();
		if(IsEnterPressed(ch))
			ExecuteCommand();
		if(IsBackspacePressed(ch))
		{
			printf("\b \b"); // Sieht aus wie ein Smily sorgt aber dafür dass das letzte zeichen gelöscht wird (Weird ich weiß)
			int length = cmdLine.length();
			cmdLine = cmdLine.substr(0,length-1);
		}	
		else
		{
			cmdLine += ch;
			printf("%c",ch);
		}	
	}
	net.HandlePackets();
}

/**
 * \brief Prüft plattformunabhängig ob Enter gedrückt wurde
 */
bool ServerMain::IsEnterPressed(char key)
{
#ifdef WIN32
	if(key == '\r')
#else
	if(key == '\n')
#endif
		return true;
	else
		return false;
}

/**
 * \brief Prüft plattformunabhängig ob Backspace gedrückt wurde
 */
bool ServerMain::IsBackspacePressed(char key)
{
#ifdef WIN32
		if(key == 8)
#else
		if(key == 127)
#endif
			return true;
		else
			return false;
}

void ServerMain::BroadcastMovePacket(Int32 plrID, Int32 cmd)
{
	ENetPacket* enetPacket;
	Packet outPacket;
	outPacket << cmd;
	outPacket << plrID;
	enetPacket = enet_packet_create(outPacket.GetData(),outPacket.GetDataSize(),ENET_PACKET_FLAG_RELIABLE);
	enet_host_broadcast(net.GetHost(),0,enetPacket);
}

void ServerMain::HandlePacket(Packet packet)
{
	int id;
	packet >> id;
	if(id == ServerProtocol::REQ_RemoveCircleFromTerrain)
	{
		ENetPacket* enetPacket;
		Vector2 pos;
		int radius;
		Packet outPacket;
		
		// Read the Client Packet
		packet >> pos >> radius;

		// Write Broadcast Packet
		outPacket << ServerProtocol::CMD_RemoveCircleFromTerrain;
		outPacket << pos.X << pos.Y << radius;
		enetPacket = enet_packet_create(outPacket.GetData(),outPacket.GetDataSize(),ENET_PACKET_FLAG_RELIABLE);
		enet_host_broadcast(net.GetHost(),0,enetPacket);
	}
	if(id == ServerProtocol::REQ_MoveDown || id == ServerProtocol::REQ_MoveLeft || id == ServerProtocol::CMD_MoveRight || id == ServerProtocol::CMD_MoveUp || id == ServerProtocol::CMD_MoveStop)
	{
		Int32 plrID;
		packet >> plrID;
		if(id == ServerProtocol::REQ_MoveDown)
			BroadcastMovePacket(plrID,ServerProtocol::CMD_MoveDown);
		if(id == ServerProtocol::REQ_MoveLeft)
			BroadcastMovePacket(plrID,ServerProtocol::CMD_MoveLeft);
		if(id == ServerProtocol::REQ_MoveRight)
			BroadcastMovePacket(plrID,ServerProtocol::CMD_MoveRight);
		if(id == ServerProtocol::REQ_MoveUp)
			BroadcastMovePacket(plrID,ServerProtocol::CMD_MoveUp);
		if(id == ServerProtocol::REQ_MoveStop)
			BroadcastMovePacket(plrID,ServerProtocol::CMD_MoveStop);
	}

}

void ServerMain::ExecuteCommand()
{
	printf("\n");
	if(cmdLine == "exit")
		net.Shutdown();
	else if(cmdLine == "help")
		PrintHelp();
	else
		LogWriteError("%s: Command not found!",cmdLine.c_str());
	cmdLine = "";
}

void ServerMain::PrintHelp()
{
	printf("\n-- Help --\n");
	printf("- exit => Beendet das Programm\n");
	printf("- help => Zeigt diese Liste an\n");
	printf("--> Die Liste ist im moment unvollstaendig!\n\n");
}
