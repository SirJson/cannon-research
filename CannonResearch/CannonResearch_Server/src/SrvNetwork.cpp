/*************************
 * Cannon Research Server
 *************************/

#include "SrvNetwork.h"
#include "ServerConfig.h"
#include "Logger.h"
#include "ServerMain.h"
#include "NetProtocol.h"
#include "DataTypes.h"

SrvNetwork::SrvNetwork()
{
	server = NULL;
	running = false;
	updateLoopRunning = false;
	updateWait = 0;
}

void SrvNetwork::Shutdown()
{
	LogWrite("Shutdown Network...");
	if(server != NULL)
		enet_host_destroy(server);
	running = false;
}

void SrvNetwork::HandleConnect(ENetPeer* peer, Uint32 host, Uint16 port)
{
	LogWrite("A new client connected from %x:%u",host,port);
	LogWrite("Sending Landscape Data...");
	Packet packet;
	packet << ServerProtocol::CMD_LandscapeTransmission;
	packet << serverMain->GetContourGenerator()->GetTerrainContour(); // Uh wenn das geht fress ich'n Stock X_x
	ENetPacket* outPacket = enet_packet_create(packet.GetData(),packet.GetDataSize(),ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer, 0, outPacket);

	/**
	 * Gib dem neuen Client seine ID
	 */
	Packet infoPacket;
	infoPacket << ServerProtocol::INFO_ConnectionData;
	infoPacket << peer->connectID;
	ENetPacket* outInfoPacket = enet_packet_create(infoPacket.GetData(),infoPacket.GetDataSize(),ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer, 0, outInfoPacket);

	/**
	 * Informiere alle anderen Clienten �ber den Neuling
	 */
	Packet broadcastPacket;
	broadcastPacket << ServerProtocol::INFO_ClientConnected;
	broadcastPacket << peer->connectID;
	ENetPacket* outBroadcastPacket = enet_packet_create(broadcastPacket.GetData(),broadcastPacket.GetDataSize(),ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer, 0, outBroadcastPacket);
	enet_host_broadcast(server,0,outBroadcastPacket);
}

void SrvNetwork::HandleDisconnect(ENetPeer* peer, Uint32 host, Uint16 port)
{
	LogWrite("%x:%u disconnected",host,port);

	/**
	 * Informiere alle Clienten dar�ber das ein Spieler aus dem Spiel entfernt wurde
	 */
	Packet broadcastPacket;
	broadcastPacket << ServerProtocol::INFO_ClientDisconnected;
	broadcastPacket << peer->connectID;
	ENetPacket* outBroadcastPacket = enet_packet_create(broadcastPacket.GetData(),broadcastPacket.GetDataSize(),ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer, 0, outBroadcastPacket);
	enet_host_broadcast(server,0,outBroadcastPacket);
}

void SrvNetwork::Init(ServerMain* serverPtr)
{
	updateWait = serverPtr->GetConfig().GetIntValue("time_waitForPackets");
	LogWrite("Set \"time_waitForPackets\" to %i",updateWait);
	serverMain = serverPtr;
	enet_initialize();
	serverAddress.host = ENET_HOST_ANY;
	serverAddress.port = NET_PORT;
	server = enet_host_create(&serverAddress,32,2,0,0);
	running = true;
	updateLoopRunning = true;
	LogWrite("Bound on Port %i",NET_PORT);
	workerThread = boost::thread(&SrvNetwork::UpdateLoop,this);
}

void SrvNetwork::UpdateLoop()
{
	while(updateLoopRunning)
	{
		ENetEvent event;
		int result = enet_host_service(server, &event, updateWait);
		if(result > 0)
		{
			if(event.type == ENET_EVENT_TYPE_CONNECT)
				HandleConnect(event.peer,event.peer->address.host,event.peer->address.port);
			if(event.type == ENET_EVENT_TYPE_DISCONNECT)
				HandleDisconnect(event.peer,event.peer->address.host,event.peer->address.port);
			if(event.type == ENET_EVENT_TYPE_RECEIVE)
				packetQueue.push(event.packet);
		}
		if(result < 0)
			LogWriteError("Event dispatch failed!");
	}
}

void SrvNetwork::HandlePackets()
{
	if(!packetQueue.empty() && running)
	{
		ENetPacket* enetPacket = packetQueue.front();
		Packet packet;
		packet.SetData((char*)enetPacket->data,enetPacket->dataLength);
		serverMain->HandlePacket(packet);
		enet_packet_destroy(enetPacket);
		packetQueue.pop();
	}
}
