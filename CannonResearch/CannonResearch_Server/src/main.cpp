/*************************
 * Cannon Research Server
 *************************/

#include "Logger.h"
#include "ServerConfig.h"
#include "ServerMain.h"

int main(int argc, char** argv)
{
	InitLogger("server.log");
	LogWrite(SERVER_NAME);
	ServerMain srv;
	while(srv.IsRunning())
	{
		srv.Update();
	}
	LogWrite("Goodbye!");
}