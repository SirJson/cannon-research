/*************************
 * Cannon Research Server
 *************************/

#ifndef SERVER_H
#define SERVER_H

#include <string>
#include "SrvNetwork.h"
#include "ContourGenerator.h"
#include "Config.h"

class ServerMain
{
public:
    ServerMain();
	~ServerMain();
	bool IsRunning();
	void Update();
	ContourGenerator* GetContourGenerator() { return landscape; }
	Config GetConfig() { return config; }
	void HandlePacket(Packet packet);
private:
	void BroadcastMovePacket(Int32 plrID, Int32 cmd);
	bool IsEnterPressed(char key);
	bool IsBackspacePressed(char key);
	void ExecuteCommand();
	void PrintHelp();
	
	Config config;
	std::string cmdLine;
	ContourGenerator* landscape;
	SrvNetwork net;
};

#endif // SERVER_H
