/*************************
 * Cannon Research Server
 *************************/

#ifndef NETWORK_H
#define NETWORK_H

#include <enet/enet.h>
#include <string>
#include "Packet.h"
#include "DataTypes.h"
#include <boost/thread.hpp>
#include <queue>

class ServerMain;

class SrvNetwork
{
    public:
		SrvNetwork();
		void Init(ServerMain* serverPtr);
		void Connect(std::string ip);
		bool IsRunning() { return running; }
		void HandlePackets();
		void Shutdown();
		ENetHost* GetHost() { return server; }
    protected:
    private:
		
		void HandleConnect(ENetPeer* peer, Uint32 host, Uint16 port);
		void HandleDisconnect(ENetPeer* peer, Uint32 host, Uint16 port);
		void UpdateLoop();

		ServerMain* serverMain;
		ENetHost* server;
		ENetAddress serverAddress;
		bool running;
		boost::thread workerThread;
		std::queue<ENetPacket*> packetQueue;
		bool updateLoopRunning;
		int updateWait;
};

#endif // NETWORK_H
