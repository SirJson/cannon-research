/*************************
 * Cannon Research Server
 *************************/

#ifndef SERVERCONFIG_H
#define SERVERCONFIG_H

#define SERVER_NAME "Cannon Research Server"
#define NET_PORT 3108 // Zu Dokumentationszwecken der Port ist durch das Datum entsanden an dem diese Datei erstellt wurde ;o
#define CONNECTION_TIMEOUT 5000 // Connection Timeout in ms

#endif //SERVERCONFIG_H