#ifndef INPUT_H
#define INPUT_H

#ifndef WIN32
int getch();
int kbhit();
#else
#include <conio.h>
#endif

#endif
